'use strict';

var assert=require('assert');

module.exports = function() {
	this.World = require('../support/world.js').World;

	this.Given(/^Open browser and Start Application one$/, function () {
		this.driver.get("http://timetrackingwip.prdxnstaging.com/");
	});

	this.Then(/^I click the add new user button$/, function () {
		this.driver.findElement({ id: 'email' }).sendKeys("sachin.yadav.prdxn@gmail.com");
		this.driver.findElement({ id: 'password' }).sendKeys("prdxn2017");

		this.driver.findElement({ css: 'input.submit-btn' }).then(function(ele){
			var click = ele.click();
		});
 
		this.driver.findElement({ css: 'span' }).click(); 
		this.driver.findElement({ id: 'admin-user' }).click();
		this.driver.sleep(500); 
		this.driver.findElement({ id: 'fname' }).clear(); 
		this.driver.findElement({ id: 'fname' }).sendKeys("test");
		this.driver.findElement({ id: 'lname' }).clear(); 
		this.driver.findElement({ id: 'lname' }).sendKeys("test");
		// this.driver.findElement({ xpath: "//select[@id='designation']//option[2]" }).isSelected().then(function(isSelected){  
	    // if(!isSelected){  
	      this.driver.findElement({ xpath: "//select[@id='designation']//option[2]" }).click();
	    // } 
		// });
		this.driver.findElement({ id: 'qualification' }).clear(); 
		this.driver.findElement({ id: 'qualification' }).sendKeys('test');
		this.driver.findElement({ id: 'address' }).clear(); 
		this.driver.findElement({ id: 'address' }).sendKeys('test');
		this.driver.findElement({ id: 'mobile_no' }).clear(); 
		this.driver.findElement({ id: 'mobile_no' }).sendKeys("0000000000");
		this.driver.findElement({ id: 'alt_no' }).clear(); 
		this.driver.findElement({ id: 'alt_no' }).sendKeys("0000000000");
		this.driver.findElement({ id: 'joining_date' }).click();
		this.driver.sleep(500);
		this.driver.findElement({ linkText: '6' }).click();
		this.driver.findElement({ id: 'email' }).clear(); 
		this.driver.findElement({ id: 'email' }).sendKeys('abcd@xyz.com');
		this.driver.findElement({ id: 'password' }).clear(); 
		this.driver.findElement({ id: 'password' }).sendKeys('prdxn2017');
		this.driver.findElement({ id: 're-password' }).clear(); 
		this.driver.findElement({ id: 're-password' }).sendKeys('prdxn2017');
		this.driver.findElement({ xpath: "//select[@id='role']//option[3]" }).click();
		this.driver.findElement({ xpath: "//input[@class='add-user-btn']" }).click();
		this.driver.sleep(3000);
	});
};