'use strict';

var assert=require('assert');

module.exports = function() {
	this.World = require('../support/world.js').World;

	this.Given(/^Open browser and Start Application five$/, function () {
		this.driver.get("http://timetrackingwip.prdxnstaging.com/");
	});

	this.Then(/^I click the record absence button$/, function () {
		this.driver.findElement({ id: 'email' }).sendKeys("sachin.yadav.prdxn@gmail.com");
		this.driver.findElement({ id: 'password' }).sendKeys("prdxn2017");

		this.driver.findElement({ css: 'input.submit-btn' }).then(function(ele){
			var click = ele.click();
		});
		
		this.driver.findElement({ css: 'span' }).click(); 
		this.driver.findElement({ linkText: 'RECORD ABSENCE' }).click(); 
		this.driver.sleep(500);
		this.driver.findElement({ id: 'select2-select-user-container' }).click();
		this.driver.sleep(1000); 
		this.driver.findElement({ xpath: "//span[@class='select2-results']/ul/li[3]" }).click();
		this.driver.sleep(1000);  
		this.driver.findElement({ id: 'start_date' }).click();
		this.driver.sleep(500); 
		this.driver.findElement({ linkText: '22' }).click();
		this.driver.sleep(500);
		this.driver.findElement({ id: 'end_date' }).click();
		this.driver.sleep(500);
		this.driver.findElement({ linkText: '29' }).click();
		this.driver.sleep(500);
		this.driver.findElement({ css: 'button.submit-data' }).click();
		this.driver.sleep(3000);
	});

};